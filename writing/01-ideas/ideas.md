# Team Members

* Keefer Sands
* Joshua Stephenson-Losey

# Existing Data integration into SQL databases
Using data that is already existing to pull into a database to do advanced queries for data analysis
(CSV, etc)
# Teaching A novice sql
Teach a novice how to access an existing database to show some cool insights about the project

# Building graphs using sql
We could teach how to make a front end web interface that accesses the database directly and executes queries and builds
graphs based on those queries
